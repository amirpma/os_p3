#include <iostream> 
#include <unistd.h> 
#include <string>
#include <vector>
#include <fstream>
#include <sys/stat.h>  
#include <sys/wait.h>
#include <map>
#include <thread>
#include <chrono>
#include <semaphore.h>

#define INPUT_FILE_NAME "inputFile"

using namespace std;
using namespace std::chrono;

unsigned long randstate = 1;
double totalEmission;
sem_t totalEmissionLock;

class Road {
public:
    string start, end;
    int h;
    Road(string start_, string end_, int h_) {
        start = start_;
        end = end_;
        h = h_;
    }
};

class PathCarNumber {
public:
    vector<string> path;
    int numberOfCars;
};

class SubPathInfo {
public:
    string entranceNode;
    string exitNode;
    string entranceTime;
    string exitTime;
    int emission;
    int totalEmission;

    string getInfoString() {
        return entranceNode + ',' + entranceTime + ',' + exitNode + ',' + exitTime + ',' + to_string(emission) + ',' + to_string(totalEmission);
    }
};

class Monitor {
public:
    
    
    vector<sem_t> locks;
    vector<Road*> roads;
    
    Monitor(vector<Road*> &roads_) {
        roads = roads_;
        for(int i = 0; i < roads.size(); i++) {
            sem_t mutex;
            sem_init(&mutex, 0, 1);
            locks.push_back(mutex);
        }
    }

    ~Monitor() {
        for(int i = 0; i < locks.size(); i++) {
            sem_destroy(&(locks[i]));
        }
    }

    void driveCar(SubPathInfo* info, int p, string startNode, string exitNode) {
        int h;
        int roadIndex = getRoadIndex(startNode, exitNode, &h);
        sem_wait(&(locks[roadIndex]));
        info->entranceTime = getTime();
        int emission = calculateEmission(h, p);
        info->exitTime = getTime();
        sem_post(&(locks[roadIndex]));
        info->emission = emission;
        info->entranceNode = startNode;
        info->exitNode = exitNode;
        sem_wait(&totalEmissionLock);
        totalEmission += emission;
        info->totalEmission = totalEmission;
        sem_post(&totalEmissionLock);
    }

    int calculateEmission(int h, int p) {
        double sum = 0;
        for(int k = 0; k <= 10000000; k++) {
            sum += (int)(((double)k) / ((double)(1000000 * p * h)));
        }
        return sum;
    }

    int getRoadIndex(string startNode, string endNode, int *h) {
        for(int i = 0; i < roads.size(); i++) {
            if((roads[i]->start == startNode) && (roads[i]->end == endNode)) {
                *h = roads[i]->h;
                return i;
            }
        }
        return -1;
    }

    string getTime() {
        unsigned long milliseconds_since_epoch = duration_cast<milliseconds> (system_clock::now().time_since_epoch()).count();
        return to_string(milliseconds_since_epoch);
    }
};

void printInFile(int pathNumber, int carNumber, vector<SubPathInfo*> &information) {
    ofstream file(to_string(pathNumber) + '-' + to_string(carNumber));
    for(int i = 0; i < information.size(); i++) {
        file << information[i]->getInfoString() << endl;
    }
}

int getRandom() {
    srand (time(NULL));
    randstate = randstate * 1664525 + 1013904223;
    return (rand() + randstate) % 10 + 1;
}

void threadFunction(int pathNumber, int carNumber, vector<string> path, Monitor* monitor) {
    vector<SubPathInfo*> information;
    int p = getRandom();
    for(int i = 0; i < path.size()-1; i++) {
        SubPathInfo* info = new SubPathInfo();;
        monitor->driveCar(info, p, path[i], path[i+1]);
        information.push_back(info);
    }
    printInFile(pathNumber, carNumber, information);
}

void createThreads(Monitor* monitor, vector<PathCarNumber*> &pathCarNumbers) {
    vector<thread*> threads;
    for(int i = 0; i < pathCarNumbers.size(); i++) {
        for(int j = 0; j < pathCarNumbers[i]->numberOfCars; j++) {
            thread* t = new thread(threadFunction, i, j, pathCarNumbers[i]->path, monitor);
            threads.push_back(t);
        }
    }
    for(int i = 0; i < threads.size(); i++) {
        threads[i]->join();
    }
}

Road* splitRoad(string s) {
    string start = "", end = "", hString = "";
    int h, seenDashes = 0;
    for(int i = 0; i < s.length(); i++) {
        if(s[i] == '-') {
            seenDashes++;
            continue;
        }
        if(seenDashes == 0)
            start += s[i];
        if(seenDashes == 1)
            end += s[i];
        if(seenDashes == 2)
            hString += s[i];
    }
    h = stoi(hString);
    return new Road(start, end, h);
}

PathCarNumber* splitPath(string s) {
    PathCarNumber* paths = new PathCarNumber();
    string temp = "";

    for(int i = 0; i < s.length(); i++) {
        if(s[i] == '-') {
            paths->path.push_back(temp);
            temp = "";
            continue;
        }
        temp += s[i];
    }
    paths->path.push_back(temp);
    
    return paths;
}

void readInputs(vector<Road*> &roads, vector<PathCarNumber*> &pathCarNumbers) {
    string command;
    bool isRoadDefine = true, needNumber = false;
    PathCarNumber* tempPathCarNumber;
    ifstream inputFile(INPUT_FILE_NAME);

    while(getline(inputFile, command)) {
        if(command == "#") {
            isRoadDefine = false;
            continue;
        }
        if(isRoadDefine)
            roads.push_back(splitRoad(command));
        else {
            if(!needNumber) {
                tempPathCarNumber = splitPath(command);
                needNumber = true;
            }
            else {
                tempPathCarNumber->numberOfCars = stoi(command);
                pathCarNumbers.push_back(tempPathCarNumber);
                needNumber = false;
            }
        }
        
    }
}

int main() {
    vector<Road*> roads;
    vector<PathCarNumber*> pathCarNumbers;

    sem_init(&totalEmissionLock, 0, 1);

    readInputs(roads, pathCarNumbers);

    Monitor* monitor = new Monitor(roads);

    createThreads(monitor, pathCarNumbers);

    sem_destroy(&totalEmissionLock);

    return 0;
}