CC=g++
STD=-std=c++11
CF=$(STD)

all: clean os_p3.o

os_p3.o: os_p3.cpp
	$(CC) $(CF) -pthread os_p3.cpp -o os_p3.out

clean:
	rm -r -f outputFolder
	rm -rf *.o *.out &> /dev/null